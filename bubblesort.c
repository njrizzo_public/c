#include<stdio.h>
#include<stdlib.h> 
#define MAXT 1000
 
void bubbleSort( char *vet, int n ){
    int i, j, aux;
 
    for( i=0; i < n; i++ )    {
        for( j = 0; j < n-1; j++ ){
            if( vet[j] > vet[j+1] ){
                aux = vet[j];
                vet[j] = vet[j+1];
                vet[j+1] = aux;
            }
        }
    }
}
int main(){
   char vetor[MAXT];
    int n, i;
 
    printf( "Quantos elementos (MAX = %d)? ", MAXT );
    scanf( "%d", &n );
   // n=n+4;
    for( i=0; i < n; i++ ){
        scanf("%c",  &vetor[i] );
    }
    bubbleSort( vetor, n );
    printf( "\nOrdenando o vetor:\n" );
    for( i=0; i < n; i++ ){
		printf("\n");
        printf( "%c", vetor[i] );
    }
    return 0;
}

