#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>

#ifndef PI
#define PI		3.1415
#endif

int	qw,qh,ww,wh,qx,qy;
double	wr,qr,rr;

void redesenha(int w, int h);
void mouse_move(int x, int y);
void mouse(int b, int s,int x, int y);
void idle(void);

int main(int argc, char** argv) {
int 		tx,ty;
int		width,height;

	// Inicializa as variaveis
	tx=ty=0;
	width=height=100;
	qw=100;qh=50;
	ww=wh=100;
	wr = ww / wh;
	qr = qw / qh;
	rr = wr / qr;
	tx = (800 - width) / 2;
	ty = (600 - height) / 2;

	glutInit(&argc, argv);	 					// inicia a biblioteca GLUT
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//	sistema de cor rgb e
														//buffer simples
	glutInitWindowPosition(tx,ty);			// Centraliza janela

	glutInitWindowSize(width,height);		// Define o Tamanho da Janela
	glutCreateWindow (".: Objetos :.");		//cria e rotula
	glEnable(GL_DEPTH_TEST);
	//  Funcoes de CallBacks
	glutMouseFunc(mouse);
	glutReshapeFunc(redesenha);
	glutMotionFunc(mouse_move);
	glutIdleFunc(idle);
	// Fim CallBacks
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glOrtho (0,ww,0,wh,-10,10); 			// especifica o sistema de 
	glViewport(qx,qy,qw,qh);
																//coordenadas
	glutMainLoop();										// Aguarda eventos (infinito)
	return 0;												// Fim
}

void mouse(int b, int s, int x, int y){
}

void mouse_move(int x, int y){
}

void idle(void){

	printf ( "Quadrado [ %d %d %lf ] Janela [ %d %d %lf ]\r",
				qw,qh,(double)(qw/qh),ww,wh,(double)(ww/wh));
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glOrtho (0,ww,0,wh,-10,10); 			// especifica o sistema de 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
		glViewport(qx,qy,qw,qh);
		glColor3f(1.,1.,1.);
		glRectf(qx,qy,qw,qh);
	glPopMatrix();
	glutSwapBuffers();
	glFlush();
}

void redesenha(int w, int h) {
double	lwr, lqr;
int		lqw, lqh;
int 		mw, mh;


	if ( w >= ( qr * h )) {
		qh = h;
		qw = h * qr;
		qx = (w -qw)/2;
		qy = 0;
	} else
	if ( h >= ( w / qr )) {
		qw = w;
		qh = w / qr;
		qx = 0;
		qy = (h -qh)/2;
	}
	
	ww=w; wh=h;
}
