#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include <unistd.h>


int main (void);

typedef char TELA;
int LINHA,COLUNA;

#define MAX_COLUNA (80)
#define MAX_LINHA  (23)


#define E_NOERR (0x00)
#define E_NOMEM (0x01)
char *err_msg[256];

void inicializa_erros(void);
TELA *iniciar_tela(void);
void desenha_eixos( TELA *t);
void atualiza_tela( TELA *t);
char get_char_xy(TELA *t, int x, int y);
void fill_char_xy(TELA *t, char c, int x, int y);
char get_char_xy(TELA *t, int x, int y);
void desenha_quadrado(int x1,int y1,int x2,int y2, TELA *t);

void inicializa_erros(void)
{
int i;
	for (i=0; i <256 ; i++)
		if (( err_msg[i] = (char *)(calloc(80,sizeof(char *)))) == (char *)(NULL)) {
			perror("Erro na inicializacao dos erros");
			exit (-255);
		}
	strcpy(err_msg[0],"Execucao Sem Erros\0");
	strcpy(err_msg[1],"Erro de Inicializacao da Tela\0");
		
}


TELA * iniciar_tela(void)
{
TELA *t;

	if ((t = (TELA *)(calloc(MAX_COLUNA * MAX_LINHA, sizeof(TELA * )))) == (char *)(NULL))
		return (char * )(NULL);
	else
		return (t);
}

void desenha_eixos( TELA *t)
{
int l,c,cf,lf;

	cf = lf =0;
	for (l = 0; l < MAX_LINHA; l++)
		for (c = 0; c < MAX_COLUNA; c++) {
			t[l * MAX_COLUNA +c] = ' ';
			if ( c == (int) ( MAX_COLUNA / 2)) {
				cf = 1;
				t[l * MAX_COLUNA + c] ='|';
			}
			if ( l == (int) (MAX_LINHA /2)) {
				lf = 1;
				t[l * MAX_COLUNA +c] = '-';
			}
			if ( cf &&  lf ) 
				t[l * MAX_COLUNA + c] = '+';
			cf = lf = 0;
		}
}

void atualiza_tela( TELA *t)
{
int c,l;

	for (l = 0; l < MAX_LINHA; l++) {
		for (c = 0; c < MAX_COLUNA; c++)
			putchar(t[l * MAX_COLUNA +c]);
		putchar('\n');
	}

}

void put_char(TELA *t, char c)
{
	t[LINHA *MAX_COLUNA + COLUNA] = c;
	COLUNA ++;
	if ( COLUNA > MAX_COLUNA ) {
		LINHA ++;
		COLUNA = 0;
	}
	if ( LINHA > MAX_LINHA )
		LINHA = MAX_LINHA;
}

void put_char_xy(TELA *t, char c, int x, int y)
{
	if ( x > (int)(MAX_COLUNA/2))
		x =  (int)(MAX_COLUNA/2);
	if ( y >  (int)(MAX_LINHA/2) )
		y =  (int)(MAX_LINHA/2);
	if ( x < -(int)(MAX_COLUNA/2))
		x =  -(int)(MAX_COLUNA/2);
	if ( y < -(int)(MAX_LINHA/2) )
		y =  -(int)(MAX_LINHA/2);
	x += (int)(MAX_COLUNA/2);
	y = MAX_LINHA  - y - (int)(MAX_LINHA /2)-1;
	LINHA = y;
	COLUNA = x;
	put_char(t,c);
	
}

void fill_char_xy(TELA *t, char c, int x, int y)
{
int xi,yi;
int dir_x=0, dir_y=0,dir_xi;
int count=0;
int LIMITE = 3;

	xi = x; yi = y;
	dir_xi = dir_x;
	do {
		count = 0;
		put_char_xy(t,c,x,y);
		x = x + ( dir_x ? 1 : -1 );
		while ( get_char_xy(t,x,y) != ' ' && count < LIMITE ) {
			if ( count == 0) {
				dir_x ^=1;
				y += (dir_y ? -1:1);
			}
			x = x + ( dir_x ? 1 : -1 );
			count ++;
		}
		if (count >= LIMITE ) {
			x = xi;y=yi;
			dir_y++;
			dir_x=dir_xi ^ 1;
		}
	} while ( dir_y < 2) ;
}

char get_char_xy(TELA *t, int x, int y)
{
	if ( x > (int)(MAX_COLUNA/2))
		x =  (int)(MAX_COLUNA/2);
	if ( y >  (int)(MAX_LINHA/2) )
		y =  (int)(MAX_LINHA/2);
	if ( x < -(int)(MAX_COLUNA/2))
		x =  -(int)(MAX_COLUNA/2);
	if ( y < -(int)(MAX_LINHA/2) )
		y =  -(int)(MAX_LINHA/2);
	x += (int)(MAX_COLUNA/2);
	y = MAX_LINHA  - y - (int)(MAX_LINHA /2)-1;
	LINHA = y;
	COLUNA = x;
	return ( t[ x + MAX_COLUNA * y] );
}

void desenha_quadrado(int x1,int y1,int x2,int y2, TELA *t)
{
int x,y;


	if ( x1 > x2) {
		x = x2;
		x2 = x1;
		x1 = x;
	}
	if ( y1 > y2 ) {
		y = y2;
		y2 = y1;
		y1 = y;
	}
	if ( x1 >= MAX_COLUNA ) 
		x1 = MAX_COLUNA - 1;
	if ( x2 >= MAX_COLUNA ) 
		x2 = MAX_COLUNA ;
	if ( y1 >= MAX_LINHA ) 
		y1 = MAX_LINHA - 1;
	if ( y2 >= MAX_LINHA ) 
		y2 = MAX_LINHA ;
	for (x=x1;x < x2;x++) {
		put_char_xy(t,'*',x,y1);
		put_char_xy(t,'*',x,y2);
	}
	for (y=y1;y < y2;y++) {
		put_char_xy(t,'*',x1,y);
		put_char_xy(t,'*',x2,y);
	}
}

int main(void)
{
/*
**  O programa tem como objetivo levar ao aluno
**  a compreender  o  funcionamento  basico  da 
**  computacao grafica, utilizando a  linguagem
**  de programacao C e o modo de texto
**
**  Os passos para este programa funcionar sao:
**
**  1 - Iniciar a tela texto para escrita em um
**  matriz virtual de C colunas por  L  linhas, 
**  usualmente esses valores  sao:  C=80,  L=25
**  2 - Desenhar as funcoes nesta tela  virtual
**  e entao atualiza-la para a tela real
**
**  Copyright Prof. Nilton Jose Rizzo
**  Estacio de Sa 20060208
**
**  Essa ferramenta  pode  ser  distribuida  ou
**  utilizada da forma em que se apresenta, sem
**  modificacoes ou a retirada o  copyright  do
**  autor
**
*/
TELA *tela;
int x,y;

	inicializa_erros();
	if ( (tela = iniciar_tela()) == (TELA *)(NULL) ) {
		perror(err_msg[E_NOMEM]);
		exit E_NOMEM;
	}
	desenha_eixos(tela);

	desenha_quadrado( -2,-5,10,10,tela);
	fill_char_xy(tela,'M',0,0);

	atualiza_tela(tela);
	return 0;
}
