/*
**
** Programa para exemplifica��o de uso do controle
** de loop while
**
**   while ( express�o ){
**     <bloco de comandos>
**   }
**
**		A express�o deve retornar um valor diferente
**		de zero( ou falso) para que o loop termine
**
**
** Include das bibliotecas padr�o do C
*/
#include <stdio.h>
#include <stdlib.h>

/*
** Prototype de fun��es
*/
int main(void);

/*
** Fun��o principal:
**   � nela que conter� todas as chamadas/procedimentos
**   necess�rios para a execu��o completa do programa
*/
int main(void){
char   *nome="Nilton Jose Rizzo\0";


	while( *nome ) {
		putchar(*nome);
		putchar('\n');
		nome++;
	}
	return 0;
}
