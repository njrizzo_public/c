#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_BUFFER		(255)
struct _FILA_	{

	unsigned int index;
	char 			 *nome;
	struct _FILA_  *next;
};

int readline(char *l,unsigned int tam, char del);

struct _FILA_ *cria_fila(void);
int				atualiza_fila(struct _FILA *f,char *l);
int				cria_elemento(struct _FILA *f);
int				mostra_fila(struct _FILA *f);
int				mostra_elemento(struct _FILA_ *f);


int main(void);


int main(void) {

char	line[MAX_LINE_BUFFER];

	inicio = fila =cria_fila();
	do {
		printf("Digite um nome: ");
		readline(line,MAX_LINE_BUFFER,':');
		atualiza_fila(fila,line);
		cria_elemento(fila);
		fila = fila->next;
		printf("\nProximo nome? <S/n>? ");
		readline(line,1,' ');
	} while ( line[0] != 'n' && line[0] != 'N' ) ;
	
}
