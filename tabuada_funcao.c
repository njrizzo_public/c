#include <stdio.h>
#include <stdlib.h>

#define MAX_VET	10

int main(void);
int *alocando(int p);

int *alocando(int p) {
int *v;
        if ((v=(int *)(malloc(sizeof(int )*p)))==(int *)(NULL)){
                printf("Erro de alocacao");
                exit(-1);
        }
	return (v);
}

int main(void) {
	int i,s,*vet;

	printf("Digite o valor: ");
	scanf("%d",&s);
	vet = alocando( MAX_VET );
	for(i=0;i<MAX_VET;i++) {
		*(vet+i) = i * s;
		printf("%d x %d = %4d\n",s,i,*(vet+i));
	}
	return(s);
}
