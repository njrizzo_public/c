/*
**
** Programa para exemplifica��o de uso do controle
** de loop do .. while
**
**   do {
**     <bloco de comandos>
**   }while ( express�o );
**
**		A express�o deve retornar um valor diferente
**		de zero( ou falso) para que o loop termine
**
**
** Include das bibliotecas padr�o do C
*/
#include <stdio.h>
#include <stdlib.h>

/*
** Prototype de fun��es
*/
int main(void);

/*
** Fun��o principal:
**   � nela que conter� todas as chamadas/procedimentos
**   necess�rios para a execu��o completa do programa
*/
int main(void){
char   *nome="Nilton Jose Rizzo\0";

	do {
		putchar(*nome);
		putchar('\n');
		nome++;
	} while( *nome ) ;
	return 0;
}

