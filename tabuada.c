#include <stdio.h>
#include <stdlib.h>

#define MAX_VET	10

int main(void);

int main(void) {
	int i,s,vet[MAX_VET];

	printf("Digite o valor: ");
	scanf("%d",&s);
	for(i=0;i<MAX_VET;i++) {
		vet[i] = i * s;
		printf("%d x %d = %4d\n",s,i,vet[i]);
	}
	return(s);
}
