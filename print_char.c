#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]);
int todecimal(char *n);

int main(int argc, char *argv[]) { 
int c;
   
   if (argc < 2) {
	printf("Error: usage %s arguments\n",argv[0]);
	exit(-1);
   }
   for (c=1; c < argc; c++)
	printf("%c",todecimal(argv[c]));
   return 0; 
} 

int todecimal(char *n){
int v,d1,d2,d3;

	*(n+1)='0';
	d3 = atoi(n);
	d2 = d3 /10;
	d1 = d3 - (d2*10);
	v = d2 * 16 + d1;
	return v;
}
