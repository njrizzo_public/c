/*
**
** Programa para Desenhar Esferas e movimenta-las de orbitas
**	simulando o sistema solar
**
** 2013-04-28
** $Id$ 
** $Author$
** $Date$
** 
*/
#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef __LIB_X11__
#include "display_info.h"

#endif

#ifndef PI
#define PI		3.1415
#endif

unsigned int	debug_on=0;
GLuint	textura[10];

#define			WIDTH			640
#define			HEIGHT		450

void desenha(int v);
void desenha_eixos(int z);
void desenha_corredor(void);
void desenha_esfera(int raio, int r, int g, int b, int w);
void desenha_quadrado(int xi,int yi,int xf,int yf,int z,int r,int g,int b);
void inicializa_opengl(int argc,char **argv,int w,int h,int x,int y,char *t);
void loadimage_raw(unsigned char *,char *fn, int tw, int th);
void loadtextura(char *fn, int tw, int th, GLuint t, int wrap);
int main(int argc, char** argv);

int main(int argc, char** argv) {
int width,height,pos_x,pos_y;
int texwidth, texheight;			//Largura e altura da textura
int ortho=0;
int wrap	= 1;
#ifdef __LIB_X11__
_display_info	di;
#endif

#ifdef __LIB_X11__
	display_info(&di);
	pos_x = (di.width - height ) / 2;
	pos_y = (di.height - height ) / 2;
#else
	pos_x = 300;
	pos_y = 300;
#endif
	width = WIDTH;
	height = HEIGHT;
#ifdef FREEBSD
	srandomdev();
#else
	srandom(34532542);
#endif
	inicializa_opengl(argc, argv,width,height,pos_x,pos_y,".: Jogo v 0.1 :.");
	glLoadIdentity();
	if ( ortho )
		glOrtho (-2*width, 2*width, -2*height, 2*height, -2*width ,2*width);
	else
		glFrustum(-width,width,-height,height,width,width*2);
																// especifica o sistema de 
																// coordenadas necessario 
																//	redesenhar
	texwidth = 128;
	texheight = 128;
	glGenTextures(1,&textura[0]);
	loadtextura("texturas/chao.raw",texwidth,texheight,textura[0],1);
	glGenTextures(1,&textura[1]);
	loadtextura("texturas/parede1.raw",texwidth,texheight,textura[1],1);
	glutMainLoop();										// Aguarda eventos (infinito)
	return 0;												// Fim
}

void loadimage_raw(unsigned char *data,char *fn, int tw, int th){
FILE *fp;
long int tam;

	tam=tw * th * 3;
	if ((fp = fopen( fn, "rb" )) == (FILE *)(NULL)){
		printf("Erro na carga do arquivo [ %s ]\n",fn);
		exit(-1);
	}
	fread( data, tam, 1, fp );
		if ( ferror(fp) ) {
		printf("Erro na leitura do arquivo [ %s ]\n",fn);
		exit(-1);
	}
	fclose( fp );
}

void loadtextura(char *fn, int tw, int th, GLuint t, int wrap){
unsigned char *data;

	if((data=(unsigned char *)(malloc(tw*th*3)))==(unsigned char *)(NULL)){		// RGB
		printf("Erro de alocacao de textura");
		exit(-1);
	}
	loadimage_raw(data,fn, tw, th);
	glBindTexture(GL_TEXTURE_2D,t);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    // when texture area is small, bilinear filter the closest mipmap
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                     GL_LINEAR_MIPMAP_NEAREST );
    // when texture area is large, bilinear filter the first mipmap
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                     wrap ? GL_REPEAT : GL_CLAMP );
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                     wrap ? GL_REPEAT : GL_CLAMP );
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, tw, th,
                   GL_RGB, GL_UNSIGNED_BYTE, data );
	free(data);
}

void inicializa_opengl(
					int argc, char **argv, 
					int w, int h, 
					int x, int y,
					char *t){

int var;

	glutInit(&argc, argv);	 							// inicia a biblioteca GLUT
	var = GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH;	//	sistema de cor rgb e
																// buffer duplo (evita flicks)
	glutInitDisplayMode ( var );
	glutInitWindowPosition(x,y);						// Centraliza janela

	glutInitWindowSize(w,h);							// Define o Tamanho da Janela
	glutCreateWindow (t);								// Cria e rotula a Janela
	glMatrixMode(GL_PROJECTION);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
/*
**   Funcoes de CallBacks
*/
/*
	glutMouseFunc(mouse);
	glutMotionFunc(mouse_motion);
	glutKeyboardFunc(teclado);
	glutIdleFunc(desenha);
*/
	glutDisplayFunc(desenha);							// Funcao para exibir quando
	glutTimerFunc(100,desenha,1);
/*
**		Fim CallBacks
*/
}

void desenha(int v) {
int i;
static int x=-WIDTH,y,z;
int static alfa=0,inc=1,ix=1,iy=2,iz=1;

		if ( inc ) {
			x=(random()&0x3FF)-WIDTH;
			y=(random()&0x1FF)-HEIGHT;
			z=-(3*WIDTH/2);
			inc = 0;
		}
		x += ix; y += iy; z += iz;
		if ( (x + 50 ) >= WIDTH ) 			ix = -1;
		if ( (y + 50 ) >= HEIGHT ) 		iy = -2;
		if ( abs(z - 50 ) >= 2*WIDTH )	iz =  1;
		if ( (x - 50 ) <= -WIDTH ) 		ix =  1;
		if ( (y - 50 ) <= -HEIGHT ) 		iy =  2;
		if ( abs(z + 50 ) <= WIDTH ) 		iz = -1;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		desenha_eixos(-WIDTH);
		desenha_corredor();
		glPushMatrix(); 
		glTranslated(x,y,z);
		desenha_esfera(50,255,255,0,1);		// Desenha uma esfera em 0,0 de cor
															// Amarela com raio 100px
		glPopMatrix();
		glPushMatrix(); 
		glTranslated(x-50,y+50,z);
		desenha_esfera(10,255,0,0,0);		// Desenha uma esfera em 0,0 de cor
															// Vermelha com raio 10px
		glPopMatrix();
  		glRasterPos2f(0, HEIGHT-50);
  		glutBitmapString(GLUT_BITMAP_HELVETICA_18, "2013 - V 0.1");

		glutSwapBuffers();
		if ( debug_on ) {
			printf("[ %d ] [ %d ] [ %d ] [ %d ]\n",x,y,z,alfa);
			glFlush(); // descarrega o buffer
		}
	glutTimerFunc(100,desenha,1);
	
}

void desenha_corredor(void){

	// Comeca pela parede esquerda, chao, parede direita, teto
	glPushMatrix();
	glColor3d(0,255,0);
	glBindTexture(GL_TEXTURE_2D,textura[1]);
	glBegin(GL_QUADS);		// Parede Esquerda
		glTexCoord2d(0,0);
		glVertex3d(-WIDTH,-HEIGHT,-WIDTH);
		glTexCoord2d(1,0);
		glVertex3d(-WIDTH,-HEIGHT,-(2 * WIDTH));
		glTexCoord2d(1,1);
		glVertex3d(-WIDTH, HEIGHT,-(2 * WIDTH));
		glTexCoord2d(0,1);
		glVertex3d(-WIDTH, HEIGHT,-WIDTH);
	glEnd();
	glPopMatrix();
	glPushMatrix();
	glColor3d(255,255,255);
	glBindTexture(GL_TEXTURE_2D,textura[0]);
	glBegin(GL_QUADS);		// Chao
		glTexCoord2d(0,0);
		glVertex3d(-WIDTH,-HEIGHT,-WIDTH);
		glTexCoord2d(2,0);
		glVertex3d( WIDTH,-HEIGHT,-WIDTH);
		glTexCoord2d(2,1);
		glVertex3d( WIDTH,-HEIGHT,-(2 * WIDTH));
		glTexCoord2d(0,1);
		glVertex3d(-WIDTH,-HEIGHT,-(2 * WIDTH));
	glEnd();
	glPopMatrix();
	glPushMatrix();
	glColor3d(0,255,0);
	glBindTexture(GL_TEXTURE_2D,textura[1]);
	glBegin(GL_QUADS);		// Parede Direita
		glTexCoord2d(0,0);
		glVertex3d( WIDTH,-HEIGHT,-(2 * WIDTH));
		glTexCoord2d(1,0);
		glVertex3d( WIDTH,-HEIGHT,-WIDTH);
		glTexCoord2d(1,1);
		glVertex3d( WIDTH, HEIGHT,-WIDTH);
		glTexCoord2d(0,1);
		glVertex3d( WIDTH, HEIGHT,-(2 * WIDTH));
	glEnd();
	glPopMatrix();
	glPushMatrix();
	glColor3d(255,255,255);
	glBindTexture(GL_TEXTURE_2D,textura[1]);
	glBegin(GL_QUADS);		// Teto
		glTexCoord2d(0,0);
		glVertex3d( WIDTH, HEIGHT,-WIDTH);
		glTexCoord2d(1,0);
		glVertex3d(-WIDTH, HEIGHT,-WIDTH);
		glTexCoord2d(1,2);
		glVertex3d(-WIDTH, HEIGHT,-(2 * WIDTH));
		glTexCoord2d(0,2);
		glVertex3d( WIDTH, HEIGHT,-(2 * WIDTH));
	glEnd();
	glPopMatrix();

}

void desenha_eixos(int z){
int c;

	glPushMatrix();
		glColor3d(255,255,0);
		glBegin(GL_LINES);
			glVertex3d(-WIDTH,0,z);
			glVertex3d(WIDTH,0,z);
		glEnd();
		glBegin(GL_LINES);
			glVertex3d(0,-WIDTH,z);
			glVertex3d(0,WIDTH,z);
		glEnd();
		glBegin(GL_LINES);
			glVertex3d(0,0,-WIDTH);
			glVertex3d(0,0,WIDTH);
		glEnd();
	glPopMatrix();
}

void desenha_esfera(int raio, int r, int g, int b, int w){

	glColor3d(r,g,b);
	if ( w )
		glutWireSphere((double)(raio),32,32);
	else
		glutSolidSphere((double)(raio),32,32);
}

void desenha_quadrado(int xi, int yi, int xf, int yf, int z, int r, int g, int b){

	glColor3d(r,g,b);
	glBegin(GL_QUADS);
		glVertex3d(xi,yi,z);
		glVertex3d(xf,yi,z);
		glVertex3d(xf,yf,z);
		glVertex3d(xi,yf,z);
	glEnd();
}

/*
void desenha_cubo(float xp, float yp, float zp, float offset)
{
float px,py,pz,nx,ny,nz;

	px = xp + offset;
	py = yp + offset;
	pz = zp + offset;
	nx = -xp + offset;
	ny = -yp + offset;
	nz = -zp + offset;
	glBegin(GL_QUADS);			// Face posterior
	glColor3f (r,g,b);			// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, 1.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(nx, py, pz);
		glVertex3f(nx, ny, pz);
		glVertex3f(px, ny, pz);
	glEnd();
	glBegin(GL_QUADS);			// Face frontal
	glColor3f (b,r,g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, -1.0); 	// Normal da face
		glVertex3f(px, py, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral esquerda
	glColor3f (g,b,r);						// Desenhando eixo X em vermelho
		glNormal3f(-1.0, 0.0, 0.0); 	// Normal da face
		glVertex3f(nx, py, pz);
		glVertex3f(nx, py, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, ny, pz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral direita
	glColor3f (1-r,1-g,1-b);						// Desenhando eixo X em vermelho
		glNormal3f(1.0, 0.0, 0.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(px, ny, pz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face superior
	glColor3f (1-b,1-r,1-g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 1.0, 0.0);  	// Normal da face
		glVertex3f(nx, py, nz);
		glVertex3f(nx, py, pz);
		glVertex3f(px, py, pz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face inferior
	glColor3f (1-g,1-b,1-r);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, -1.0, 0.0); 	// Normal da face
		glVertex3f(nx, ny, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, ny, pz);
		glVertex3f(nx, ny, pz);
	glEnd();
//	glutSwapBuffers();
}

int my_cor(float c) {

	return ((int)(255 * c));

}
		case	GL_INVALID_ENUM:
										printf("TEXTURA: Invalid ENUM\n");
										break;
		case	GL_INVALID_VALUE:
										printf("TEXTURA: Invalid VALUE\n");
										break;
		case	GL_INVALID_OPERATION:
										printf("TEXTURA: Invalid OPERATION\n");
										break;
	}
*/
