#include<stdio.h>
 
#define MAXT 1000
int main()
{
    int vetor[MAXT];
    int n, i;
 
    printf( "Quantos elementos (MAX = %d)? ", MAXT );
    scanf( "%d", &n );
 
    for( i=0; i < n; i++ )
    {
        scanf("%d", &( vetor[i]) );
    }
//bubbleSort( vetor, n );
 
    printf( "\nOrdenando o vetor:\n" );
    for( i=0; i < n; i++ )
    {
        printf( "%d ", vetor[i] );
    }
    return 0;
}
