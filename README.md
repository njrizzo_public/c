# What's This?


This are some example programs to teaching programming to my students.
I'm using C language to teaching because I think it's have more ease  to improve the programming skill of them.
All program start by exemplo_[object will study].c, inside of them are some conde and comments in pt_BR language,
easy to translate and use in other language.

I'm try to cover all basic programming skills, like a logical programming, variables, loop controls, input and output methods, and much more.

Try this with your students and feel free to change or add some new code. If you wish shared with me your changes I'll appreciate.

All code was under BSD licence.

