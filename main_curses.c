#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <curses.h>

int main(void);

int main(void) {

	WINDOW	*win;
	SCREEN	*scr;

	int 		colorido;
	unsigned int pares;

	win = initscr();
	start_color();
	colorido = has_colors();
	if (colorido)
		pares = COLOR_PAIRS-1;
/**
	box( win, '|','-');
**/
	box( win, 0,0);
	refresh();
/**
	getch();	
**/
	clear();
	refresh();
	endwin( );	
	printf("Terminal %s possui cores [ %u ]\n",colorido?"":"nao",pares);
	return(0);
}
