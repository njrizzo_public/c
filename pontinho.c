#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void);

int main(void){
int cont=0;
int i,dir;
char *ch="|\\-/|";

	puts("\f");
	dir = 0;
	i=0;
	while(1) {
		printf("...");
		cont=0;
		while (cont <10){
			printf("%c\b",ch[i]);
			fflush(stdout);
			sleep(1);
			cont ++;
			if ( dir )
				i--;
			else
				i++;
			if ((i > 3) && !dir ) i = 0;
			if ((i < 1) &&  dir ) i = 4;
		}
		dir ^= 1;
		printf("\r");
	}
	return 0;
}
