/*
**
** Programa para exemplifica��o de uso do controle
** de loop for
**
**		for( Express�o_inicial, Express�o_teste, Express�o_loop)
**
**		Express�o:
**			Inicial:  Inicializa variaveis, condi��es iniciais do loop
**			Loop: Geralmente incremento/decremento da vari�vel de controle
**					mas pode conter outras express�es
**			Teste: � a express�o respons�vel pelo controle do loop, ou seja
**					� o ponto de parada, enquanto for verdadeira, o loop prosegue
**
**
** Include das bibliotecas padr�o do C
*/
#include <stdio.h>
#include <stdlib.h>

/*
** Prototype de fun��es
*/
int main(void);

int main(void){
int counter;


	for(counter = 0;counter<5; counter++) {
		printf("Counter = %d\n",counter);
	}
	for(int counter = 5;counter>0; counter--) {
		printf("Counter = %d\n",counter);
	}
	for (int flag=1; flag; ){
		flag = ++counter % 10;
		printf("Flag = %d\n", flag);
	}
	return 0;
}

