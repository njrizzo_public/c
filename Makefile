UNAME!=uname -n

CPPFLAGS += --std=c++14 -Wunused-value
CFLAGS   += -Wunused-value
INCLUDE_DIR +=-I/usr/local/include -I/usr/include -I.
LIBS_DIR +=-L/usr/lib -L. -L/usr/local/lib
LIBS+= -lm
OUTPUT = puts_clang puts_clang.s printf_clang printf_clang.s puts_gcc puts_gcc.s printf_gcc printf_gcc.s 
OUTPUT += exemplo_do exemplo_for exemplo_while exemplo_opengl exemplo_printf exemplo_bubblesort
TOUCH= a.core a.o teste $(OUTPUT)

all: ${OUTPUT}

.cpp.o:

	clang++ -o $@ -c $< $(INCLUDE_DIR) $(CPPFLAGS)

.c.o:

	clang -o $@ -c $< $(INCLUDE_DIR) $(CFLAGS)


printf_clang.s: printf_clang.c
	clang -c $> -S -o printf_clang.s
printf_clang: printf_clang.c
	clang $> -o printf_clang
puts_clang.s: puts_clang.c
	clang -c $> -S -o puts_clang.s
puts_clang: puts_clang.c
	clang $> -o puts_clang

printf_gcc.s: printf_gcc.c
	gcc -c $> -S -o printf_gcc.s
printf_gcc: printf_gcc.c
	gcc $> -o printf_gcc
puts_gcc.s: puts_gcc.c
	gcc -c $> -S -o puts_gcc.s
puts_gcc: puts_gcc.c
	gcc $> -o puts_gcc

exemplo_do: exemplo_do.o 
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS)
exemplo_for: exemplo_for.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS)
exemplo_while: exemplo_while.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS)
exemplo_opengl: exemplo_opengl.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS_GL) $(LIBS)
exemplo_printf: exemplo_printf.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS)
exemplo_bubblesort: exemplo_bubblesort.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS)
exemplo01: exemplo01.o
	clang -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR) $(LIBS_DIR) $(LIBS_GL) $(LIBS)
	

clean:

	@echo -n "CLeaning  ${OUTPUT} ... "
	@touch ${OUTPUT}
	@- rm ${OUTPUT}
	@echo "Done"
