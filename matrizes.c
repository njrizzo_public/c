/*
** Programa para exemplos de soma e multiplicacao
** de matrizes
**
**  $id
**
*/

#include <stdio.h>	/* Arquivos de Inclusao Padrao (I/O e Libs) */
#include <stdlib.h>
#include <ctype.h>	/* Arquivo de Inclusao para funcoes gerais como toupper */


int main(void);		/* Funcao principal - Obvio */

void mostra_matriz( double *M, int y, int x, char *n,int p);
/*
** Funcao para mostrar uma matriz M(y,x) 
** Nome:
**		mostra_matriz
** Parametros:
**
**	Double *M		- Matriz em forma de vetor
** Int y,x			- Linha e coluna da Matriz M (dimensoes)
**	Char *n			- Caso queira mostrar o Nome da matriz ou outra msg generica
** int p				- Precisao para mostrar o conteudo de M 
**
*/
double *multiplica_matrizes(double *AA, int i, int j, double *BB, int k, int l) ;
/*
** Funcao para multiplicar uma matriz M(y,x) 
** Nome:
**		multiplica_matrizes
** Parametros:
**
**	Double *AA		- Matriz em forma de vetor
** Int i,j			- Linha e coluna da Matriz AA (dimensoes)
**	Char *n			- Caso queira mostrar o Nome da matriz ou outra msg generica
** int p				- Precisao para mostrar o conteudo de M 
**
*/
double *soma_matrizes(double *AA, int i, int j, double *BB, int k, int l) ;



int main(void) {

double	*A, *B, *C;
int		i,j,k,l,m,n,a,b,c,d; /* A(i,j), B(k,l), C(m,n) a,b,c,d contadores */
double	valor,sum;
char		op;


	printf("Digite as dimensoes da matriz A(i,j): ");
	scanf("%d %d",&i,&j);
	printf("\nMatriz A(%2d,%2d)\n",i,j);

	printf("Digite as dimensoes da matriz B(k,l): ");
	scanf("%d %d",&k,&l);
	printf("\nMatriz B(%2d,%2d)\n",k,l);

	printf("Alocando Matrizes ....\n");

	if ((A = (double *)(malloc(sizeof(double)*j*i)))==(double *)(NULL)) {
		printf("Erro de Alocacao Matriz A\n");
		exit(0);
	}

	for (c=0;c<i;c++)
		if ((B = (double *)(malloc(sizeof(double)*k*l)))==(double *)(NULL)) {
			printf("Erro de Alocacao Matriz B\n");
			free(A);
			exit(0);
		}

	printf("Matrizes alocadas\n");

	printf("Digite a operacao (S)oma ou (M)ultiplicacao : ");
	do {
		scanf("%c",&op);
		if ( ( op != 'S' && op != 's')  &&
		  	( op != 'M' && op != 'm') )  {
			printf("\b \b");
			op = ' ';
		}
	}
	while( op == ' ' );

	for (a=0; a < i; a++)
		for (b=0; b< j; b++) {
			printf("Digite o elemento A(%2d,%2d) : ",a,b);
			scanf("%lf",&valor);
			*(A+a*j+b) = valor;
		}


	for (a=0; a < k; a++)
		for (b=0; b< l; b++) {
			printf("Digite o elemento B(%2d,%2d) : ",a,b);
			scanf("%lf",&valor);
			*(B+a*l+b) = valor;
		}

	mostra_matriz( A, i,j,"A",2) ;
	mostra_matriz( B, k,l,"B",2) ;


	op = toupper(op);
	if ( op == 'S' ) {
		C = soma_matrizes(A,i,j,B,k,l);
		mostra_matriz (C,i,j,"C",2);
	}
	else {
		C = multiplica_matrizes(A,i,j,B,k,l);
		mostra_matriz (C,i,l,"C",2);
	}
	free(A);free(B),free(C);
}


double *soma_matrizes(double *AA, int i, int j, double *BB, int k, int l) {
double *C;
int a,b,c;

		printf("Somando as Matrizes\n");
		if ( i != k || j !=l ) {
			printf("Erro: Dimensoes das matrizes diferentes\n");
			free(AA);free(BB);
			exit(0);
		}
		if ((C = (double *)(malloc(sizeof(double)*j*i)))==(double *)(NULL)) {
			printf("Erro de Alocacao Matriz C\n");
			free(AA);free(BB);
			exit(0);
		}
		for (a = 0;a < i; a ++)
			for( b = 0; b < j; b++) 
				*(C+a*j+b) = *(AA+a*j+b) + *(BB+a*j+b);
		return (C);
}


double *multiplica_matrizes(double *AA, int i, int j, double *BB, int k, int l) {
double *C;
int a,b,c;

		printf("Multiplicando as Matrizes\n");
		if ( j != k ) {
			printf("Erro: Dimensoes das matrizes diferentes\n");
			free(AA);free(BB);
			exit(0);
		}
		if ((C = (double *)(malloc(sizeof(double)*i*l)))==(double *)(NULL)) {
			printf("Erro de Alocacao Matriz C\n");
			free(AA);free(BB);
			exit(0);
		}
		for ( a=0; a<i; a++)
			for( b=0; b<l; b++) {
				*(C+a*l+b)=0.0;
				for ( c=0; c<j; c++)
					*(C+a*l+b) += *(AA+a*j+c) * *(BB+c*l+b);
				}
		return (C);
}

void mostra_matriz( double *M, int y, int x, char *n,int p) {
int a,b;

	for (a=0; a < y; a++)
		for (b=0; b< x; b++) 
			printf("%s(%2d,%2d) = %.*lf\n",n,a,b,p,*(M+a*x+b));

}
