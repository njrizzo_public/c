#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#include "GL/glut.h"

#define		WIDTH		600
#define		HEIGHT		600

int	counter=0, thread_alive;

int main(int argc, char *argv[]);

void exibir(void);
void teclado(unsigned char tecla, int x, int y);
void rodar(float angulo,char *eixo);
int my_cor(float c);
void desenha_cubo(float xp, float yp, float zp, float offset);
void mouse_move(int x, int y);
void mouse(int b, int s,int x, int y);
void idle(int i);


pthread_t tid[2];

void* doSomeThing(void *arg)
{
	if ( thread_alive ) {
		unsigned long i = 0;
		pthread_t id = pthread_self();

		if(pthread_equal(id,tid[0]))
		{
			printf("\n First thread processing\n");
			counter ++;
		}
		else
		{
			printf("\n Second thread processing\n");
		}
	}
	else
		printf("\n Thread not alived\n");
	while(1)
		counter++;
    return NULL;
}



int main(int argc, char *argv[]){
int		width,height;

    int i = 0;
    int err;

    while(i < 1)
    {
        err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
        else
            printf("\n Thread created successfully\n");

        i++;
    }

	thread_alive = 1;

	width=height=WIDTH;
	glutInit(&argc, argv);	 									// inicia a biblioteca GLUT
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);	//	sistema de cor rgb e
																//buffer simples
	glutInitWindowPosition(0,0);								// Centraliza janela

	glutInitWindowSize(width+20,height+20);							// Define o Tamanho da Janela
	glutCreateWindow (".: Visualizacao :.");					//cria e rotula
	//  Funcoes de CallBacks
	glutMouseFunc(mouse);
	glutMotionFunc(mouse_move);
	glutKeyboardFunc(teclado);
//	glutIdleFunc(idle);
	glutTimerFunc(100,idle,1);
	// Fim CallBacks
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glPushMatrix();
	// especifica o sistema de coordenadas
	glOrtho (-20, width, -20, height, 0 ,width); 
	glutDisplayFunc(exibir);									// Funcao para exibir quando
																// necessario redesenhar
	thread_alive = 1;
	glutMainLoop();												// Aguarda eventos (infinito)

//	glEnable(GL_DEPTH_TEST);
		
	return(0);


}


void mouse(int b, int s, int x, int y){

	if (b == 0 && s==0) {
	}
	if (b == 0 && s == 1) {
	}
	if (b == 3 && s == 1) {
	}
	if (b == 4 && s == 1 ){
	}

}

void mouse_move(int x, int y){

}

void idle(int i){
static float dx,dy,dz;
static float angulo=0;
static long long int wait=0;
unsigned int limite;

	exibir();
	glutTimerFunc(100,idle,1);
	printf("%10d\r",counter);
	fflush(stdout);
}

void exibir(void)
{
int i,c;
struct _fecho *f;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f((random()%1000)/(random()%10000),1.,0.);
	glBegin(GL_POINTS);
	i = random()%100;
		for(c=0;c<i;c++)
			glVertex3d(random()%WIDTH, random()%HEIGHT,0);
	glEnd();

	glutSwapBuffers();
	glFlush(); // descarrega o buffer
}


void teclado(unsigned char tecla, int x, int y){

	fflush(stdout);
}

int my_cor(float c) {

	return ((int)(255 * c));

}


