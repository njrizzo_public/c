#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef __DEC_TO_BIN__
#define		T_INT			(0)
#define		T_LINT		(1)
#define		T_LLINT		(2)
#define		T_UINT		(4)
#define		T_ULINT		(8)
#define		T_ULLINT		(16)
#endif

int main(void);
char *dec2bin(int t,...);

int main(void) {

unsigned 				int	un1;
unsigned long 			int	un2;
unsigned long long 	int	un3;
signed					int	sn1;
signed	long 			int	sn2;
signed	long long 	int	sn3;

unsigned 				int	num;
							int 	snum,
									opt;


	do {
		printf("\
=================================\n\
=== Selecione o Tipo do Valor ===\n\
=================================\n\
\n\
	(1) - Inteiro Sem sinal\n\
	(2) - Inteiro Longo Sem Sinal\n\
	(3) - Inteiro Comprido Sem Sinal\n\
	(4) - Inteiro Com Sinal\n\
	(5) - Inteiro Longo Com Sinal\n\
	(6) - Inteiro Comprido Com Sinal\n\
\n\
	(0) - Termino\n\
\n\
Opcao :");
/* scanf("%d",&opt); */
		opt=-1;
		while (opt < 0 || opt > 6)
			opt = getchar() - '0';
		if (opt)
			printf("\nOpcao [ %d ]\n\nDigite um numero : ",opt);
		switch(opt) {
			case 1:
				scanf("%d",&sn1);
				printf("%d [ 0x%08X ] ( %d ) %s\n", sn1, sn1, sizeof(sn1), dec2bin(T_INT,sn1));
				break;
			case 2:
				scanf("%ld",&sn2);
				printf("%ld [ 0x%08X ] ( %d ) %s\n", sn2, sn2, sizeof(sn2), dec2bin(T_LINT,sn2));
				break;
			case 3:
				scanf("%lld",&sn3);
				printf("%lld [ 0x%08X ] ( %d ) %s\n", sn3, sn3, sizeof(sn3), dec2bin(T_LLINT,sn3));
				break;
			case 4:
				scanf("%u",&un1);
				printf("%u [ 0x%08X ] ( %d ) %s\n", un1, un1, sizeof(un1), dec2bin(T_UINT,un1));
				break;
			case 5:
				scanf("%lu",&un2);
				printf("%lu [ 0x%08X ] ( %d ) %s\n", un2, un2, sizeof(un2), dec2bin(T_ULINT,un2));
				break;
			case 6:
				scanf("%llu",&un3);
				printf("%llu [ 0x%08X ] ( %d ) %s\n", un3, un3, sizeof(un3), dec2bin(T_ULLINT,un3));
				break;

		}
	}
	while(opt);
}

char *dec2bin(int t,...){

va_list		vlist;

int			tam,bits;
unsigned		int	uv;
unsigned		long int	ulv;
unsigned 	long long int	ullv,bitmask;
				int	v;
				long int	lv;
				long long int llv;


	va_start(vlist,t);
	switch(t) {
		case T_INT:
					llv = va_arg(vlist,int);
					tam = sizeof(int);
					break;
		case T_LINT:
					llv = va_arg(vlist,long int);
					tam = sizeof(long int);
					break;
		case T_LLINT:
					llv = va_arg(vlist,long long int);
					tam = sizeof(long long int);
					break;
		case T_UINT:
					llv = va_arg(vlist,unsigned int);
					tam = sizeof(unsigned int);
					break;
		case T_ULINT:
					llv = va_arg(vlist,unsigned int);
					tam = sizeof(unsigned long int);
					break;
		case T_ULLINT:
					llv = va_arg(vlist,unsigned long long int);
					tam = sizeof(unsigned long long int);
					break;
	}
	va_end(vlist);
	bits = tam*8;
#ifdef __DEBUG__
	bitmask = 0x01;
	while (bits){
		printf("0x%016llX [ %llu ] ( %d )\n",bitmask,bitmask,bits);
		bitmask <<= 1;
		bits --;
	}
	bits = tam * 8;
#endif
	bitmask = 0x01;
	bitmask <<= (bits-1);
#ifdef __DEBUG__
	printf("mascara [ 0x%016llX,%d ] Bits [ %d ] Valor [ %lld ]\n",bitmask,sizeof(bitmask), bits, llv);
#endif
	while(bits) {
		if (!(bits % 8) ) putchar(' ');
		printf("%c",((llv & bitmask)?'1':'0'));
		llv <<= 1;
		bits--;
	}
	printf("\n");
	return ("\nXXX\n");
}
