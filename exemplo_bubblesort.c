/*
**
** Programa para exemplificação de uso da ordenação BUBBLE SORT
**
** Entrada:
**   Vetor de inteiro		: vetor
**   Quantidade de valores	: n
**			Valor máximo neste exemplo ( 1000 )
**
**
** Include das bibliotecas padrão do C
*/
#include<stdio.h>
#include<stdlib.h> 

#define MAXT 1000
 
int main(void);

void bubbleSort( char *vet, int n ){
int i, j, aux;
 
    for( i=0; i < n-1; i++ ){
        for( j = i+1; j < n; j++ ){
            if( vet[i] > vet[j] ){
                aux = vet[i];
                vet[i] = vet[j];
                vet[j] = aux;
            }
        }
    }
}

int main(void){
char vetor[MAXT];
int n, i;
 
   printf( "Quantos elementos (MAX = %d)? ", MAXT );
   scanf( "%d\n", &n );
	printf("Voce digitou: %d\n",n);
   for( i=0; i < n; i++ ){
		printf("Digite o valor [%d] : ",i);
      scanf("%c\n",  &vetor[i] );
   }
   bubbleSort( vetor, n );
   printf( "\nOrdenando o vetor:\n" );
   for( i=0; i < n; i++ ){
       printf( "%c\n", vetor[i] );
   }
   return 0;
}


