/*
**
** Programa para Desenhar Pontos, Triangulos ou Quadrados
** apartir de um arquivo texto contento os valores dos 
** vertices, um por linha
**
** 2013-03-30
** $Id$ 
** $Author$
** $Date$
** 
*/
#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>

#ifndef PI
#define PI		3.1415
#endif

#define	__EMCASA__

#ifdef __EMCASA__
#define	EMCASA	(1)
#else
#define 	EMCASA	(0)
#endif


char	comando;
float	r,g,b,zoom;;
int 	mx, my,old_mx, old_my;

void exibir(void);
void teclado(unsigned tecla, int x, int y);
void rodar(float angulo,char *eixo);
int my_cor(float c);
void desenha_cubo(float xp, float yp, float zp, float offset);
void mouse_move(int x, int y);
void mouse(int b, int s,int x, int y);
void idle(void);

int main(int argc, char** argv) {
int 		tx,ty;
int		width,height;

	// Inicializa as variaveis
	r=1.0;g=1.0;b=0.0;
	mx=my=old_mx=old_my=0;
	zoom=1;
	comando = '*';
	tx=ty=0;
	width=height=512;

	tx = (1440 - width) / 2;
	ty = (900 - height) / 2;

	glutInit(&argc, argv);	 							// inicia a biblioteca GLUT
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//	sistema de cor rgb e
																//buffer simples
	glutInitWindowPosition(tx,ty);					// Centraliza janela

	glutInitWindowSize(width,height);				// Define o Tamanho da Janela
	glutCreateWindow (".: Objetos :.");			//cria e rotula
	glEnable(GL_DEPTH_TEST);
	//  Funcoes de CallBacks
	glutMouseFunc(mouse);
	glutMotionFunc(mouse_move);
	glutKeyboardFunc(teclado);
	glutIdleFunc(idle);
	// Fim CallBacks
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glPushMatrix();
	glOrtho (-2*width, 2*width, -2*height, 2*height, -2*width ,2*width); 			// especifica o sistema de 
																//coordenadas
	glutDisplayFunc(exibir);							// Funcao para exibir quando
																// necessario redesenhar
	glutMainLoop();										// Aguarda eventos (infinito)
	return 0;												// Fim
}

void mouse(int b, int s, int x, int y){

	if (b == 0 && s==0) {
		old_mx = mx = x;
		old_my = my = y;
	}
	if (b == 0 && s == 1) {
		old_mx = x;
		old_my = y;
	}
	if (b == 3 && s == 1) {
		zoom+=0.1;
		glScalef(zoom,zoom,zoom);
		exibir();
	}
	if (b == 4 && s == 1 ){
		zoom+=-0.1;
		glScalef(zoom,zoom,zoom);
		exibir();
	}
	printf("Zoom [ %f ]\n",zoom);

}

void mouse_move(int x, int y){
int lx,ly; 
static int angulo;
static int old_lx,old_ly;

	lx = x - mx;
	if ( lx > old_lx )
   	angulo = +1;
	else
		angulo = -1;
	printf("x=%d y=%d angulo=%d\n",x,y,angulo);
	rodar(angulo,"Y");
	exibir();
	old_ly=ly;
	ly = y - my;
	if ( ly > old_ly )
   	angulo = +1;
	else
		angulo = -1;
	printf("x=%d y=%d angulo=%d\n",x,y,angulo);
	rodar(angulo,"X");
	exibir();
}

void idle(void){
static float dx,dy,dz;
static float angulo=0;
static long long int wait=0;
unsigned int limite;


	if ( EMCASA )
		limite = 10000;
	else
		limite = 100000;

	if ( wait > limite) {
		wait = 0;
		dx += 1.0;
		dy = 3 * dx;
		dz += ( dx / 4 + dy / 10 );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glPushMatrix();
			glRotatef(angulo,1.0,1.0,1.0);
			glTranslatef(dx,dy,dz);
			desenha_cubo(100.0,100.0,100.0,0.0);
		glPopMatrix();
		glPushMatrix();
			glRotatef(-(angulo*1.5),1.0,1.0,1.0);
/*
		glTranslatef(-dx,-dy,-dx);
*/
			desenha_cubo(100.0,100.0,100.0,200.0);
		glPopMatrix();
		glPushMatrix();
			glColor3f(1.,1.,1.);
			glRotatef(angulo,0.,1.,0.);
			glTranslatef(-200.0,0.,0.);
			glutSolidSphere(100,32,32);
		glPopMatrix();
		glPushMatrix();
			glColor3f(0.,0.,1.);
			glRotatef(angulo,0.,1.,1.);
			glTranslatef(-300.-300,0.,0.);
			glutWireTeapot(300);
			glutSolidTeapot(100);
		glPopMatrix();
		glutSwapBuffers();
		glFlush();
		angulo += 1;
		//if (angulo > 360 ) angulo =0;
		if ( dx > 512 ) dx = 1;
		if ( dy > 512 ) dy = 1;
		if ( dx > 512 ) dz = 1;
	}
	wait ++;
	
}
void exibir(void)
{
int i;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glTranslatef(-200.,-200,-200);
	desenha_cubo(100.0,100.0,100.0,0.0);
	glPopMatrix();
	glPushMatrix();
	glRotatef(20.0,1,1,1);
	desenha_cubo(100.0,100.0,100.0,200.0);
	glPopMatrix();

  	glColor3f(1.0,0.0,0.0); 
  	glRasterPos2f(0, 500);
  	glutBitmapString(GLUT_BITMAP_HELVETICA_18, "2013 - V 0.1");

	glutSwapBuffers();
	glFlush(); // descarrega o buffer
}

void desenha_cubo(float xp, float yp, float zp, float offset)
{
float px,py,pz,nx,ny,nz;

	px = xp + offset;
	py = yp + offset;
	pz = zp + offset;
	nx = -xp + offset;
	ny = -yp + offset;
	nz = -zp + offset;
	glBegin(GL_QUADS);			// Face posterior
	glColor3f (r,g,b);			// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, 1.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(nx, py, pz);
		glVertex3f(nx, ny, pz);
		glVertex3f(px, ny, pz);
	glEnd();
	glBegin(GL_QUADS);			// Face frontal
	glColor3f (b,r,g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, -1.0); 	// Normal da face
		glVertex3f(px, py, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral esquerda
	glColor3f (g,b,r);						// Desenhando eixo X em vermelho
		glNormal3f(-1.0, 0.0, 0.0); 	// Normal da face
		glVertex3f(nx, py, pz);
		glVertex3f(nx, py, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, ny, pz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral direita
	glColor3f (1-r,1-g,1-b);						// Desenhando eixo X em vermelho
		glNormal3f(1.0, 0.0, 0.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(px, ny, pz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face superior
	glColor3f (1-b,1-r,1-g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 1.0, 0.0);  	// Normal da face
		glVertex3f(nx, py, nz);
		glVertex3f(nx, py, pz);
		glVertex3f(px, py, pz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face inferior
	glColor3f (1-g,1-b,1-r);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, -1.0, 0.0); 	// Normal da face
		glVertex3f(nx, ny, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, ny, pz);
		glVertex3f(nx, ny, pz);
	glEnd();
//	glutSwapBuffers();
}

void teclado(unsigned tecla, int x, int y){

static char 	eixo[3],modo[15];
float a;

	a = 0.0;

	if (eixo[0] == '\0')
	strcpy(eixo,"---");
	if ( tecla == 'h' || tecla == 'H' )
		printf("\nAjuda:\n\
Comandos:\n\n\
	<R>	- Mudar cor Vermelha ( Red )\n\
	<G>	- Mudar a cor Verde ( Green )\n\
	<B>	- Mudar a cor Azul ( Blue )\n\
	<X>	- Girar em torno do eixo X\n\
	<Y>	- Girar em torno do eixo Y\n\
	<Z>	- Girar em torno do eixo Z\n\
	<+>	- Altera o angulo em +1 ou a cor em +1 ( ate 255)\n\
	<->	- Altera o angulo em -1 ou a cor em -1 ( ate 0)\n\
	< >	- Sai do Comando (tecla espaco)\n\
	<0> .. <7> - Modo de profundidade\n\
	<H>	- Essa tela de ajuda\n\n");
		if ( comando == '*') {
			switch(tecla) {
				case 'r'		:
				case 'R'		:
									comando = 'R';
									break;
				case 'g'		:
				case 'G'		:
									comando = 'G';
									break;
				case 'b'		:
				case 'B'		:
									comando = 'B';
									break;
				case 'x'		:
				case 'X'		:
									comando = 'X';
									break;
				case 'y'		:
				case 'Y'		:
									comando = 'Y';
									break;
				case 'z'		:
				case 'Z'		:
									comando = 'Z';
									break;
				case	'0'	:
								glDepthFunc(GL_NEVER);
								strcpy(modo,"GL_NEVER");
								break;
				case	'1'	:
								glDepthFunc(GL_LESS);
								strcpy(modo,"GL_LESS");
								break;
				case	'2'	:
								glDepthFunc(GL_EQUAL);
								strcpy(modo,"GL_EQUAL");
								break;
				case	'3'	:
								glDepthFunc(GL_LEQUAL);
								strcpy(modo,"GL_LEQUAL");
								break;
				case	'4'	:
								glDepthFunc(GL_GREATER);
								strcpy(modo,"GL_GREATER");
								break;
				case	'5'	:
								glDepthFunc(GL_NOTEQUAL);
								strcpy(modo,"GL_NOTEQUAL");
								break;
				case	'6'	:
								glDepthFunc(GL_GEQUAL);
								strcpy(modo,"GL_GEQUAL");
								break;
				case	'7'	:
								glDepthFunc(GL_ALWAYS);
								strcpy(modo,"GL_ALWAYS");
								break;
				default		:
									comando = '*';
									break;
			}
		}
		else
		switch ( tecla ) {
			case '+'	:
							switch ( comando ) {
								case 'X'	:
											a = 1;
											strcpy(eixo,"X");
											break;
								case 'Y'	:
											a = 1;
											strcpy(eixo,"Y");
											break;
								case 'Z'	:
											a = 1;
											strcpy(eixo,"Z");
											break;
								case 'R'	:
											r+=0.003;
											if ( r > 1.0 ) r=1.0;
											break;
								case 'G'	:
											g+=0.003;
											if ( g > 1.0 ) g=1.0;
											break;
								case 'B'	:
											b+=0.003;
											if ( b > 1.0 ) b=1.0;
											break;
							}
							break;
			case '-'	:
							switch ( comando ) {
								case 'X'	:
											a = -1;
											strcpy(eixo,"X");
											break;
								case 'Y'	:
											a = -1;
											strcpy(eixo,"Y");
											break;
								case 'Z'	:
											a = -1;
											strcpy(eixo,"Z");
											break;
								case 'R'	:
											r-=0.003;
											if ( r < 0.0 ) r=0.0;
											break;
								case 'G'	:
											g-=0.003;
											if ( g < 0.0 ) g=0.0;
											break;
								case 'B'	:
											b-=0.003;
											if ( b < 0.0 ) b=0.0;
											break;
							}
							break;
			case ' '	:
							comando='*';
		}	
	printf("Tecla %u  [ %c ] %d %d %d (rgb) Eixo %s Modo %s   \r",
		tecla,comando,my_cor(r),my_cor(g),my_cor(b),eixo,modo);
	rodar(a,eixo);
	exibir();
	fflush(stdout);
}

int my_cor(float c) {

	return ((int)(255 * c));

}

void rodar(float angulo,char *eixo){
float x,y,z;
int 	t,i;

	t = strlen(eixo);
	x=y=z=0;
	for (i=0; i<t;i++) {
		switch( *(eixo + i) ) {
			case 'x'	:
			case 'X' :
							x=1;
							break;
			case 'y'	:
			case 'Y'	:
							y=1;
							break;
			case 'z'	:
			case 'Z'	:
							z=1;
							break;
			default:
							break;
		}
	}
	glRotatef(angulo,x,y,z);
}


/*
	glDepthRange(-width,width);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_GREATER);
*/
