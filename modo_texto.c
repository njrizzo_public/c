#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define	PI	3.141516
#define	DEBUG	0

int main(void);
char *cria_matriz(int x, int y);
void mostrar_matriz(char *m);
void liga_ponto(int x, int y, char *m,char p);
void desliga_ponto(int x, int y, char *m);
void va_para_xy(int x, int y);

int cursor_x, cursor_y;
int MAX_LIN,MAX_COL;

void liga_ponto(int x, int y, char *m, char p) {
	if ( x >= MAX_LIN || y >= MAX_COL ) printf("erro de parametros LP\n");
	else *(m+x*MAX_COL+y) = p;
}

void desliga_ponto(int x, int y, char *m) {
	if ( x >= MAX_LIN || y >= MAX_COL ) printf("erro de parametros DP\n");
	else *(m+x*MAX_COL+y) = ' ';
}

void va_para_xy(int x, int y) {
	if ( x >= MAX_LIN || y >= MAX_COL ) printf("erro de parametros VP\n");
	else { cursor_x = x; cursor_y = y; }
}

char *cria_matriz(int x, int y) {
char *M;
	if((M = (char *)(malloc(sizeof(char)*x*y)))==(char *)(NULL)) {
		printf("Erro de Alocacao de memoria\n");
		exit(-1);
	}
	else
		cursor_x = 0; cursor_y = 0;
	return (M);
}

void mostrar_matriz(char *m) {
int x,y;
	for(x=0;x<25;x++) putchar('\n');
	for(x=0;x<MAX_LIN;x++)
		for(y=0;y<MAX_COL;y++) putchar(*(m+x*MAX_COL+y));
}

void inicializa_matriz(char *m) {
int x,y;
	for(x=0;x<MAX_LIN;x++)
		for(y=0;y<MAX_COL;y++)
			*(m+x*MAX_COL+y) = ' ';
}

int main(void) {
int x,y;
double a;
int i,j;

char *tela,*env;

/**	env = getenv("LINES");
**	printf("%s\n",env);
**	MAX_LIN = atoi(env);
**	env = getenv("COLUMNS");
**	printf("%s\n",env);
**	MAX_COL = atoi(env);
**/
	MAX_LIN=67;
	MAX_COL=145;
	tela = cria_matriz(MAX_LIN,MAX_COL);
	inicializa_matriz(tela);
	x = 33;
	for(y=0;y<MAX_COL;y++)
		liga_ponto(x,y,tela,'*');
	y = 72;
	for(x=0;x<MAX_LIN;x++)
		liga_ponto(x,y,tela,'*');
	for(x=1;x<360;x++) {
		a = x * PI / 180;
		j = (int)(x/3);
		i = (int)(cos(a))*30+33;
		liga_ponto(i,j,tela,'+');
		i = (int)(sin(a))*30+33;
		liga_ponto(i,j,tela,'.');
		i = (int)(tan(a))*30+33;
		if ( x != 90 && x != 270 )
			liga_ponto(i,j,tela,'o');
#ifdef DEBUG
		printf ("%lf %d .. ",a,x);
#endif
	}
	mostrar_matriz(tela);
	free(tela);
}
