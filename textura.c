/*
**
** Programa para Testar Texturas
**	simulando o sistema solar
**
** 2013-04-28
** $Id$ 
** $Author$
** $Date$
** 
*/
#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef __LIB_X11__
#include "display_info.h"
#endif

#ifndef PI
#define PI		3.1415
#endif

GLuint	textura[10];
unsigned int	debug_on=0;

int 				x=0,y=0,z=-70,ix=1,iy=1,iz=1;

#define			WIDTH			640
#define			HEIGHT		450

void desenha0(void);
void desenha(int v);
void desenha_alvo(int x, int y);
void desenha_quadrado(int xi,int yi,int xf,int yf,int z,int r,int g,int b);
void desenha_cubo(float xp, float yp, float zp, float offset, GLuint t);
void desenha_chao(void);
void desenha_teto(void);
void desenha_parede_direita(void);
void desenha_parede_esquerda(void);
void inicializa_opengl(int argc,char **argv,int w,int h,int x,int y,char *t);
void loadimage_raw(unsigned char *,char *fn, int tw, int th);
void loadtextura(char *fn, int tw, int th, GLuint t, int wrap);
int main(int argc, char** argv);

int main(int argc, char** argv) {
int width,height,pos_x,pos_y;
int texwidth, texheight;			//Largura e altura da textura
int ortho	=0;
int wrap		=1;
int var;

#ifdef __LIB_X11__
_display_info	di;
#endif

#ifdef __LIB_X11__
	display_info(&di);
	pos_x = (di.width - height ) / 2;
	pos_y = (di.height - height ) / 2;
#else
	pos_x = 300;
	pos_y = 300;
#endif
	width = WIDTH;
	height = HEIGHT;
#ifdef __FREEBSD__
	srandomdev();
#else
	srandom(2354235);
#endif

	glutInit(&argc, argv);	 							// inicia a biblioteca GLUT
	var = GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH;	//	sistema de cor rgb e
																// buffer duplo (evita flicks)
	glutInitDisplayMode ( var );
	glutInitWindowPosition(pos_x,pos_y);			// Centraliza janela
	glutInitWindowSize(width,height);				// Define o Tamanho da Janela
	glutCreateWindow (".: Jogo :.");					// Cria e rotula a Janela
	glutSetCursor(GLUT_CURSOR_NONE);

	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	if ( ortho )
		glOrtho (-2*width, 2*width, -2*height, 2*height, -2*width ,2*width);
	else
		glFrustum(-width,width,-height,height,width,width*2);
																// especifica o sistema de 
																// coordenadas necessario 
																//	redesenhar

	glEnable(GL_DEPTH_TEST);							// Habilita Z-Buffer
	glEnable(GL_TEXTURE_2D);							// Habilita Textura

/*
**   Funcoes de CallBacks
*/
	glutDisplayFunc(desenha0);							// Funcao para exibir quando
	glutTimerFunc(100,desenha,1);
	glutPassiveMotionFunc(desenha_alvo);
/*
	glutMouseFunc(mouse);
	glutMotionFunc(mouse_motion);
	glutKeyboardFunc(teclado);
	glutIdleFunc(desenha);
*/
/*
**		Fim CallBacks
*/
	texwidth = 128;
	texheight = 128;
	glGenTextures(1,&textura[0]);
	loadtextura("texturas/chao.raw",texwidth,texheight,textura[0],1);
	glGenTextures(1,&textura[1]);
	loadtextura("texturas/parede1.raw",texwidth,texheight,textura[1],1);

	glutMainLoop();										// Aguarda eventos (infinito)
	return 0;												// Fim
}

void loadimage_raw(unsigned char *data,char *fn, int tw, int th){
FILE *fp;
long int tam;

	tam=tw * th * 3;
	if ((fp = fopen( fn, "rb" )) == (FILE *)(NULL)){
		printf("Erro na carga do arquivo [ %s ]\n",fn);
		exit(-1);
	}
	fread( data, tam, 1, fp );
	if ( ferror(fp) ) {
		printf("Erro na leitura do arquivo [ %s ]\n",fn);
		exit(-1);
	}
	fclose( fp );
}

void loadtextura(char *fn, int tw, int th, GLuint t, int wrap){
unsigned char *data;

	if((data=(unsigned char *)(malloc(tw*th*3)))==(unsigned char *)(NULL)){// RGB
		printf("Erro de alocacao de textura");
		exit(-1);
	}
	loadimage_raw(data,fn, tw, th);
	glBindTexture(GL_TEXTURE_2D,t);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
  	// when texture area is small, bilinear filter the closest mipmap
   //glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
   //                  GL_LINEAR_MIPMAP_NEAREST );
   // when texture area is large, bilinear filter the first mipmap
   //glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                     wrap ? GL_REPEAT : GL_CLAMP );
   glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                     wrap ? GL_REPEAT : GL_CLAMP );
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, tw, th,
                   GL_RGB, GL_UNSIGNED_BYTE, data );
	free(data);
}

void inicializa_opengl( int argc, char **argv, 
								int w, int h, 
								int x, int y,
								char *t){
}

void desenha(int v) {
int i;
char str[80];
static int alfa;
int tdx,tdy;

	tdy=tdx=512/4;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if ( (x + 50 ) >= WIDTH ) 			ix = -1;
	if ( (y + 50 ) >= HEIGHT ) 		iy = -2;
	if ( abs(z - 50 ) >= 2*WIDTH )	iz =  1;
	if ( (x - 50 ) <= -WIDTH ) 		ix =  1;
	if ( (y - 50 ) <= -HEIGHT ) 		iy =  2;
	if ( abs(z + 50 ) <= WIDTH ) 		iz = -1;
	x += ix; y += iy; z += iz;

	desenha_chao();
	desenha_parede_direita();
	desenha_teto();
	desenha_parede_esquerda();
	glPushMatrix();
		glColor3d(255,255,255);
  		glRasterPos3d(-WIDTH+50, -HEIGHT+30,-WIDTH);
		sprintf(str,"X[ %04d ] Y[ %04d ] Z[ %04d ] ALFA[ %04d ] T[ %d %d %d ]",
						x,y,z,alfa,textura[0],textura[1],textura[2]);
  		glutBitmapString(GLUT_BITMAP_HELVETICA_18, str);
	glPopMatrix();
	glutSwapBuffers();
	if ( debug_on ) {
		printf("%s\n",str);
		glFlush(); // descarrega o buffer
	}
	glutTimerFunc(100,desenha,1);
}

void desenha_alvo(int x, int y) {
int	alfa,px,py,pz,cx,cy;
char str[80];

	glPushMatrix();
		pz = -WIDTH;
		cx = 2*x-WIDTH;
		cy = -(2*y-HEIGHT);
		glTranslated(cx,cy,0);
		glColor3d(255,255,255);
		glBegin(GL_LINE_LOOP);
			for(alfa=0;alfa<=360;alfa+=10) {
				px = 50 * cos(alfa*PI/180);
				py = 50 * sin(alfa*PI/180);
				glVertex3d(px,py,pz);
			}
		glEnd();
		glBegin(GL_LINE_LOOP);
			for(alfa=0;alfa<=360;alfa+=10) {
				px = 10 * cos(alfa*PI/180);
				py = 10 * sin(alfa*PI/180);
				glVertex3d(px,py,pz);
			}
		glEnd();
		glBegin(GL_LINE);
			glVertex3d(-60,0,pz);
			glVertex3d(-10,0,pz);
		glEnd();
		glBegin(GL_LINE);
			glVertex3d( 10,0,pz);
			glVertex3d( 60,0,pz);
		glEnd();
		glBegin(GL_LINE);
			glVertex3d(0,-60,pz);
			glVertex3d(0,-10,pz);
		glEnd();
		glBegin(GL_LINE);
			glVertex3d(0, 10,pz);
			glVertex3d(0, 60,pz);
		glEnd();
	glPopMatrix();
	sprintf(str,"X[ %04d ] Y[ %04d ]",x,y);
	glColor3d(0,255,0);
	glRasterPos3d(-WIDTH+50, -HEIGHT+80,-WIDTH);
  	glutBitmapString(GLUT_BITMAP_HELVETICA_18, str);
	glutSwapBuffers();
	glColor3d(255,255,255);
}

void desenha_parede_esquerda(void){

	glPushMatrix(); 
		glColor3d(255,255,255);
		glBindTexture(GL_TEXTURE_2D,textura[1]);
		glTranslated(y,x,0);
		glBegin(GL_QUADS);		
			glTexCoord2d(0,0);
			glVertex3d(-200,-200,-900);
			glTexCoord2d(1,0);
			glVertex3d( 200,-200,-900);
			glTexCoord2d(1,1);
			glVertex3d( 200, 200,-900);
			glTexCoord2d(0,1);
			glVertex3d(-200, 200,-900);
		glEnd();
	glPopMatrix();
}

void desenha_parede_direita(void){

	glPushMatrix(); 
		glColor3d(255,255,0);
		glBindTexture(GL_TEXTURE_2D,textura[1]);
		glTranslated(x,y,0);
		glBegin(GL_QUADS);		
			glTexCoord2d(0,0);
			glVertex3d(-100,-100,-850);
			glTexCoord2d(1,0);
			glVertex3d( 100,-100,-850);
			glTexCoord2d(1,1);
			glVertex3d( 100, 100,-850);
			glTexCoord2d(0,1);
			glVertex3d(-100, 100,-850);
		glEnd();
	glPopMatrix();
}

void desenha_chao(void){

	glPushMatrix(); 
		glTranslated(0,0,z);
		glColor3d(0,0,255);
		glBegin(GL_QUADS);		
			glVertex3d(-50,-50,0);
			glVertex3d( 50,-50,0);
			glVertex3d( 50, 50,0);
			glVertex3d(-50, 50,0);
		glEnd();
	glPopMatrix();
}

void desenha_teto(void){

	glPushMatrix(); 
		glTranslated(0,0,z);
		glColor3d(255,255,255);
		glBegin(GL_QUADS);		
			glVertex3d( 500,300,-750);
			glVertex3d( 600,300,-750);
			glVertex3d( 600,400,-750);
			glVertex3d( 500,400,-750);
		glEnd();
	glPopMatrix();
}

void desenha0(void){

	desenha(1);

}
