#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]);

int main(int argc, char *argv[]){
int	angulo;
double a;

	puts("Angulo\t\t     sen\tcos\t tg\t   asin\t     acos\tatan\t");
	puts("  Graus    Radianos");
	for(angulo=0; angulo< 360; angulo += 45){
		a = angulo * 3.1415 / 180;
		if ( angulo == 90 || angulo == 270 )
			printf(" %6d ° %9.6lf %9.6lf %9.6lf NAOEXISTE %9.6lf %9.6lf %9.6lf \n", angulo, a, sin(a), cos(a), asin(sin(a)), acos(cos(a)), atan(tan(a)));
		else
			printf(" %6d ° %9.6lf %9.6lf %9.6lf %9.6lf %9.6lf %9.6lf %9.6lf \n", angulo, a, sin(a), cos(a), tan(a), asin(sin(a)), acos(cos(a)), atan(tan(a)));
	}
}
