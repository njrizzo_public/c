#include <stdio.h>
#include <stdlib.h>

void swapbubble( int v[], int i)
{
 
int aux=0; 
 
   aux=v[i];
   v[i] = v[i+1];
   v[i+1] = aux;
 
}
 
  void bubble(int v[], int qtd)
{
        int i;
        int trocou;
 
        do
        {
                qtd--;
                trocou = 0;
 
                for(i = 0; i < qtd; i++)
                {
                        if(v[i] > v[i + 1])
                        {
                                swapbubble(v, i);
                                trocou = 1;
                        }
                }
        }while(trocou);
}
