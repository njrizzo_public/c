/*
**
** Programa para exemplificação de uso de saida de dados
**
**		printf("Formatação"[,[expressão1[,expressão2[,...[,expressãoN]]]]])
**
**		Formatação:
**
**			c		- Valores tipo caracter							(char)
**			s		- Valores tipo caracter							(char *)
**			d		- Valores tipo inteiros em decimal			(int)
**			i		- Valores tipo inteiros							(int)
**			o		- Valores tipo inteiros em octal				(int)
**			u		- Valores tipo sem sinal						(unsigned)
**			x		- Valores tipo sem sinal em hexadecimal	(unsigned)
**			X		- Valores tipo sem sinal em hexadecimal	(unsigned)
**			f		- Valores tipo float								(float)
**			eE		- Valores tipo float (nitação cientifica)	(float)
**			p		- Valores tipo ponteiro	em hexadecimal		(void *)
**			%		- Para escrever o caracter %
**
**		Modificadores:
**			
**			h		- short
**			l		- long	(d,i,o,u,x,X - long [unsigned] int; (eE,f - double) )
**			ll		- long long
**			L		- long double
**
** Include das bibliotecas padrão do C
*/
#include <stdio.h>
#include <stdlib.h>

/*
** Prototype de funções
*/
int main(void);

int main(void){
char				c;
char				*s;
int				i;
unsigned int	ui;
long int			li;
long long int	lli;
unsigned long int	uli;
unsigned long long int ulli;
float				f;
double			d;
void 				*p;

	c = '8';
	i	= 65535<<16;
	ui = 65535<<16;
	f	= 10.8686896986;
	d  = 79.63456928452658;
	s = "Teste de tipos";

	p = main;

	printf ("Char %c  \n",c);
	printf ("Char * %s  \n",s);
	printf ("Int %d %x %X \n",i,i,i);
	printf ("Int %u %x %X \n",i,i,i);
	printf ("Float %f %lf \n",f,f);
	printf ("Double %f %lf  \n",d,d);
	printf ("Double %e %E \n",d,d);
	printf ("Pointer %p \n",p);
	return 0;
}
